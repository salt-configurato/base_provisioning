# yamllint disable-file

{% set formulae = salt['pillar.get']('core:salt:formulae',{}) %}
{% set formulae_path = salt['pillar.get']('core:salt:formulae_path','/srv/formula') %}
{% set formulae_cache = salt['pillar.get']('core:salt:formulae_cach','/srv/formula_cache') %}

create_core_salt_formulae_cache:
  file.directory:
    - name: '{{ formulae_cache }}'
    - makedirs: True

create_core_salt_formulae_path:
  file.directory:
    - name: '{{ formulae_path }}'
    - makedirs: True

{% for formula, item in formulae.items() %}
acquire_{{ formula }}_formula:
  git.cloned:
    - target: '{{ formulae_cache }}/{{ formula }}-formula'
    {{ item|yaml(false)|indent(4) }}

link_{{ formula }}_formula:
  file.symlink:
    - name: '{{ formulae_path }}/{{ formula }}'
    - target: '{{ formulae_cache }}/{{ formula }}-formula/{{ formula }}'
    - force: True
    - requires:
        - git.acquire_{{ formula }}_formula

{% endfor %}

# yamllint disable-file

install_pip:
  pkg.installed:
    - name: python3-pip

{% if salt['pillar.get']('common:packages') != "" %}
install_common_packages:
  pkg.installed:
    - pkgs: {{ salt['pillar.get']('common:packages',[]) }}
{% endif %}

{% if salt['pillar.get']('common:pip') != "" %}
install_common_pip:
  pip.installed:
    - pkgs: {{ salt['pillar.get']('common:pip',[]) }}
    - require:
        - pkg: install_pip
{% endif %}

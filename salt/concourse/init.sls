include:
  - docker

concourse-install-docker-compose:
  cmd.run:
    - name: 'curl -SL https://github.com/docker/compose/releases/download/v2.20.3/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose'
    - creates: /usr/local/bin/docker-compose
    - require_in:
        - cmd: concourse-start

concourse-install-compose-file:
  file.managed:
    - name: /opt/concourse/docker-compose.yml
    - source: salt://concourse/files/docker-compose.yml
    - template: jinja
    - makedirs: True

concourse-start:
  cmd.run:
    - name: 'docker-compose up -d'
    - cwd: /opt/concourse
    - require:
        - file: concourse-install-compose-file
        - cmd: concourse-install-docker-compose
        - service: docker-software-service-running-docker

concourse-install-fly:
  cmd.run:
    - name: 'sleep 10; curl -L "http://localhost:{{ salt['grains.get']('vagrant:concourse:guest_port') }}/api/v1/cli?arch=amd64&platform=linux" >/usr/local/bin/fly && chmod +x /usr/local/bin/fly || rm -f /usr/local/bin/fly'
    - creates: /usr/local/bin/fly
    - require:
        - cmd: concourse-start

download_and_unpack_texlive_bundle:
  archive.extracted:
    - name: /tmp/texlive
    - source: https://mirror.apps.cam.ac.uk/pub/tex-archive/systems/texlive/tlnet/install-tl-unx.tar.gz
    - source_hash: https://mirror.apps.cam.ac.uk/pub/tex-archive/systems/texlive/tlnet/install-tl-unx.tar.gz.sha512
    - source_hash_update: True
    - skip_files_list_verify: True
    - trim_output: True
    - enforce_toplevel: False
    - options: '--strip-components=1'
    - if_missing: /usr/local/texlive

install_texlive:
  pkg.installed:
    - name: perl
  file.managed:
    - name: /tmp/texlive.profile
    - source: salt://latex/files/texlive.profile
  cmd.run:
    - name: perl install-tl -profile "/tmp/texlive.profile" && echo "PATH=/usr/local/texlive/2020/bin/x86_64-linux:\$PATH" > /etc/profile.d/add_texlive.sh
    - cwd: /tmp/texlive
    - creates: /usr/local/texlive
    - requires:
      - file: install_texlive
      - pkg: install_texlive
      - archive: download_and_unpack_texlive_bundle

install_texlive_luaxml_package:
  cmd.run:
    - name: /usr/local/texlive/2020/bin/x86_64-linux/tlmgr install luaxml
    - creates: /usr/local/texlive/2020/texmf-dist/tex/luatex/luaxml
    - requires:
      - cmd: install_texlive

install_python_poetry:
  pkg.installed:
    - name: python3-venv
  cmd.run:
    - name: 'curl -sSL https://install.python-poetry.org | python3 -'
    - runas: vagrant
    - require:
        - pkg: install_python_poetry

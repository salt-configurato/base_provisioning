# Workaround until git-delta package available in Debian repo
install_git-delta:
  pkg.installed:
    - sources:
      - git-delta-musl: https://github.com/dandavison/delta/releases/download/0.16.5/git-delta-musl_0.16.5_amd64.deb

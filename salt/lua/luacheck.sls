install-lua-luacheck:
  pkg.installed:
    - name: luarocks
  cmd.run:
    - name: luarocks install luacheck
    - unless: command -v luacheck > /dev/null  2>&1
    - require:
        - pkg: install-lua-luacheck

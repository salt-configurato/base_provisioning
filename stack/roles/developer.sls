common:
  pip:
    - pre-commit
    - commitizen
    - cookiecutter
  packages:
    - git
    - vim
    - tmux
    - tree
    - ruby-full
    - build-essential
states:
  - common_packages
  - git-delta_workaround

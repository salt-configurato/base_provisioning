common:
  pip:
    - mypy
    - flake8
    - black
    - pre-commit
    - python-lsp-server[Rope,Pyflakes]
    - pyls-flake8
    - pyls-mypy
    - python-lsp-black
    - pylsp-rope
    - pytest-bdd
    - pytest
  packages:
    - python-is-python3
python:
  version: 3.8.2
states:
  - python.pyenv
  - python.poetry

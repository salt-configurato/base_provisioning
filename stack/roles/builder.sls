states:
  - docker.software.service
  - concourse
docker:
  wanted:
    - docker
    - compose
  pkg:
    docker:
      use_upstream: repo
common:
  pip:
    - docker == 5.0.3

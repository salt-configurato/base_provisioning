core:
  salt:
    formulae:
      docker:
        - name: 'https://github.com/saltstack-formulas/docker-formula.git'
      golang:
        - name: 'https://github.com/saltstack-formulas/golang-formula.git'
